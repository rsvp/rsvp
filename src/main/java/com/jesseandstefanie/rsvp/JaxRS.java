/*
 * Boilerplate required to register services.
 * Copyright (C) 2015 Jesse Bickel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.jesseandstefanie.rsvp;

import javax.ws.rs.core.Application;
import javax.ws.rs.ApplicationPath;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

@ApplicationPath("/rsvp")
public class JaxRS extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new CopyOnWriteArraySet<Class<?>>();
        s.add(Rsvp.class);
        return s;
    }
}
