/*
 * Restlet for event rsvps.
 * Copyright (C) 2015 Jesse Bickel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.jesseandstefanie.rsvp;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;

@Path("/")
public class Rsvp {

    private static final Logger LOGGER = Logger.getLogger(Rsvp.class.getName());

    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/html")
    public Response postRsvp(@FormParam("code") String code,
                           @FormParam("definitely_yes") int definitelyYes,
                           @FormParam("probably_yes") int probablyYes,
                           @FormParam("probably_no") int probablyNo,
                             @FormParam("definitely_no") int definitelyNo,
                             @FormParam("comments") String comments) {

        LOGGER.info("Params found. code = " + code +
                    ", definitely_yes = " + definitelyYes +
                    ", probably_yes = " + probablyYes +
                    ", probably_no = " + probablyNo +
                    ", definitely_no = " + definitelyNo +
                    ", comments = " + comments);

        // look for the code in the db
        // SELECT code_id, min_count, max_count FROM codes WHERE code = '"+code+"';
        DataSource ds = null;
        Map codeRow = null;
        try {
            ds = getDataSource("jdbc/rsvp_postgres");
            try (Connection con = ds.getConnection()) {
                codeRow = getCodeRowFromDb(con, code);
            } catch (SQLException se) {
                LOGGER.warning("Problem getting a code row: " + se);
                return genericInternalServerError();
            }
        } catch (NamingException ne) {
            LOGGER.warning("Problem y'all!" + ne);
            return genericInternalServerError();
        }
        // if less than one row found, throw a pretty-enough error
        if (codeRow.size() < 1) {
            LOGGER.info("No results found for code = " + code);
            return Response.status(404).entity(
                       getHtmlResponse("RSVP id " + code + " not found",
                                       "Oops!",
                                       "Could not find an RSVP code of " +
                                       code +
                                       ". Please click the Back button and correct the code entered.")
                                               ).build();
        }

        // if found, check to see if total is between (inclusive)
        // min_count and max_count
        // not critical though.

        try {
            String message;
            int minCount = (int) codeRow.get("min_count");
            int maxCount = (int) codeRow.get("max_count");
            int sum = definitelyYes + probablyYes + probablyNo + definitelyNo;
            if (sum > maxCount) {
               message = sum + " is greater than the count of " + maxCount + " invited guests.";
               return Response.status(412).entity(getHtmlResponse("Too many guests!", "Uh oh", message)).build();
            }
            if (sum < minCount) {
               message = sum + " is less than the minimum RSVP count of " + minCount + " invited guests.";
               return Response.status(412).entity(getHtmlResponse("Too few guests!", "Uh oh", message)).build();
            }
        } catch (ClassCastException e) {
            LOGGER.warning("Could not convert min or max count: " + e);
        }
        // if bad input, return bad input page

        // if good input, insert a row.

        int codeId = (int) codeRow.get("code_id");
        int rowsInserted = 0;
        // INSERT INTO rsvps (code_id, definitely_yes, probably_yes, probably_no, definitely_no)
        try (Connection con = ds.getConnection()) {
            rowsInserted = insertRsvpRowIntoDb(con,
                                               codeId,
                                               definitelyYes,
                                               probablyYes,
                                               probablyNo,
                                               definitelyNo,
                                               comments);
        } catch (SQLException se) {
            LOGGER.warning("Problem inserting data: " + se);
            return genericInternalServerError();
        }

        return Response.ok(getHtmlResponse(rowsInserted + " RSVP entered",
                                           "Thank you much!",
                                           "Thank you for your RSVP. " +
                                           "You may come back and update your response " +
                                           "as much as you like if the situation changes." +
                                           " To update your response, simply visit again " +
                                           "and enter the newest information.")
                           ).build();
    }

    private DataSource getDataSource(String name) throws NamingException {
        InitialContext context = new InitialContext();
        if (context == null) {
            throw new NamingException("Could not find context!");
        }
        DataSource ds = (DataSource) context.lookup("java:/comp/env/" + name);
        return ds;
    }

    private Map<String,Object> getCodeRowFromDb(Connection connection,
                                                String code
                                                ) throws SQLException {
        String query = "SELECT code_id, min_count, max_count FROM codes WHERE " +
            "code = '"+sanitize(code)+"' LIMIT 1";
        Map<String,Object> theResults = new ConcurrentHashMap<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet results = statement.executeQuery(query);
            if (results.next()) {
                theResults.put("code_id", results.getInt("code_id"));
                theResults.put("min_count", results.getInt("min_count"));
                theResults.put("max_count", results.getInt("max_count"));
            }
        } catch (SQLException se) {
            LOGGER.warning("Freaking SQL Exception. The query was: " +
                           query + ". The exception is: " + se);
            throw se;
        }
        return theResults;
    }


    private int insertRsvpRowIntoDb(Connection connection,
                                    int codeId,
                                    int definitelyYes,
                                    int probablyYes,
                                    int probablyNo,
                                    int definitelyNo,
                                    String comments
                                    ) throws SQLException {
        String query = "INSERT INTO rsvps (code_id, definitely_yes, " +
            "probably_yes, probably_no, definitely_no, comments) VALUES (" +
            codeId + ", " + definitelyYes + ", " + probablyYes +
            ", " + probablyNo + ", " + definitelyNo + ", '" + sanitize(comments) + "')";
        int theResultCount = -2;
        try (Statement statement = connection.createStatement()) {
            boolean result = statement.execute(query);
            if (!result) {
                theResultCount = statement.getUpdateCount();
            }
        } catch (SQLException se) {
            LOGGER.warning("Freaking SQL Exception. The query was: "
                           + query + ". The exception is: " + se);
            throw se;
        }
        return theResultCount;
    }

    private String getHtmlResponse(String title, String h1, String body) {
        return "<!DOCTYPE html><html><head><title>" +
            title + "</title></head><body><h1>" +
            h1 + "</h1>" +
            body + "</body></html>";
    }

    private Response genericInternalServerError() {
        return Response.serverError().
            entity(getHtmlResponse("Internal Server Error",
                                   "We messed up.",
                                   "Please try again tomorrow or phone us instead.")
                   ).build();
    }

    private String sanitize(String unsanitaryString) {
        return unsanitaryString.replace('\'','"')
            .replace(';',',')
            .replace('-','=')
            .replace((char) 0,'0')
            .replace('\r',' ')
            .replace('\n',' ')
            .replace(')',']')
            .replace('(','[');
    }
}
