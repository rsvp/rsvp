-- Database initialization program for event rsvps.
-- Copyright (C) 2015 Jesse Bickel

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.


DROP TABLE IF EXISTS codes;

CREATE SEQUENCE code_id_seq;
CREATE TABLE codes (
       code_id     integer PRIMARY KEY DEFAULT nextval('code_id_seq'),
       code        varchar(31) NOT NULL UNIQUE,
       min_count   integer NOT NULL,
       max_count   integer NOT NULL,
       is_test     boolean DEFAULT false
);
ALTER SEQUENCE code_id_seq OWNED BY codes.code_id;



DROP TABLE IF EXISTS rsvps;

CREATE SEQUENCE rsvp_id_seq;
CREATE TABLE rsvps (
       rsvp_id     integer PRIMARY KEY DEFAULT nextval('rsvp_id_seq'),
       code_id     integer NOT NULL REFERENCES codes (code_id),
       entry_date          timestamp DEFAULT CURRENT_TIMESTAMP,
       definitely_yes      smallint NOT NULL,
       probably_yes        smallint NOT NULL,
       probably_no         smallint NOT NULL,
       definitely_no       smallint NOT NULL,
       comments            varchar(255) DEFAULT ''
);
ALTER SEQUENCE rsvp_id_seq OWNED BY rsvps.rsvp_id;


-- set up permissions for dbuser
GRANT SELECT ON TABLE codes TO dbuser;
GRANT SELECT ON SEQUENCE code_id_seq TO dbuser;
GRANT INSERT, SELECT ON TABLE rsvps TO dbuser;
GRANT USAGE ON SEQUENCE rsvp_id_seq TO dbuser;
