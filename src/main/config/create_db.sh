#!/bin/bash

# Database configuration program for event rsvps.
# Copyright (C) 2015 Jesse Bickel

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# expected to be run as postgres user
# expecting postgresql to have already been installed

# usage:
# sudo apt-get install postgresql-9.4
# sudo --preserve-env su --preserve-environment -c "/bin/bash create_db.sh" -l postgres
# expecting that conf has already been modified and server restarted

# note that this program is destructive, will remove all your data

# make sure to edit /etc/postgresql/9.4/main/pg_hba.conf to have
# local   all             all                                     md5


# Destroy old database and users
dropdb "rsvp"
dropuser "dbuser"
dropuser "dbadmin"

# Generate two random passwords
passwordone=`cat /dev/urandom | tr -dc 'a-zA-Z0-9'|fold -w 80 | head -n 1`
passwordtwo=`cat /dev/urandom | tr -dc 'a-zA-Z0-9'|fold -w 80 | head -n 1`

echo "The dbuser password to enter is (make a note):"
echo ${passwordtwo}

createuser --port=5433 --no-createdb --encrypted --no-superuser --no-createrole --login --pwprompt "dbuser"

echo "The dbadmin password to enter is (make a note):"
echo ${passwordone}

# Create postgres schemas and users
createuser --port=5433 --createdb --encrypted --superuser --createrole --login --pwprompt "dbadmin"

echo "Creating rsvp database with dbadmin owner."
createdb "rsvp" --port=5433 --owner="dbadmin"

echo "Setting up rsvp database."
psql --port=5433 --dbname="rsvp" --file="create_tables.sql" --username="dbadmin" --quiet --password
