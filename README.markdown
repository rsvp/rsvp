# RSVP site

## Install dependencies

    sudo apt-get install git gnupg openjdk-7-jdk postgresql-9.4 tar wget

## Get the code

    cd ~
    git clone https://gitlab.com/rsvp/rsvp.git

## Set up the database 

### Allow connections to the database

Make sure to edit /etc/postgresql/9.4/main/pg_hba.conf to have md5 for local

    local   all             all                                     md5

Make sure to edit /etc/postgresql/9.4/main/postgresql.conf to listen on localhost and port 5433

    listen_addresses = 'localhost' 
    ...
    port = 5433

Restart if changes made

    sudo /etc/init.d/postgresql restart
  
### Create database and database users

    cd ~/rsvp/src/main/config
    sudo -E su --preserve-environment -c "/bin/bash create_db.sh" -l postgres
    
During the create_db.sh run, you will need to copy and paste two different generated passwords. Just put each password on the clipboard when it comes up, and paste it as needed.

## Compile classes and compose the war file

    cd ~/rsvp
    ./build.sh

## Set up the container

### Get the container

    cd ~
    wget http://www.gtlib.gatech.edu/pub/apache/tomcat/tomcat-8/v8.0.21/bin/apache-tomcat-8.0.21.tar.gz
    wget https://www.apache.org/dist/tomcat/tomcat-8/v8.0.21/bin/apache-tomcat-8.0.21.tar.gz.asc
    gpg --import rsvp/keys/tomcat8_keys
    gpg --verify apache-tomcat-8.0.21.tar.gz.asc && tar zxvf apache-tomcat-8.0.21.tar.gz
    
### Copy db Driver
    
    cd ~/rsvp
    cp src/main/resources/common/postgresql*.jar ~/apache-tomcat-*/lib/
    
### Add Resource to context.xml

Edit ~/apache-tomcat-*/conf/context.xml to have the following (note the REPLACE_ME...)

    <Resource name="jdbc/rsvp_postgres" auth="Container"
              type="javax.sql.DataSource" driverClassName="org.postgresql.Driver"
              url="jdbc:postgresql://127.0.0.1:5433/rsvp"
              username="dbuser" 
              password="REPLACE_ME_WITH_WHAT_WAS_GENERATED_EARLIER_FOR_DBUSER" 
              maxTotal="20" maxIdle="10" maxWaitMillis="-1"/>

## Deploy the war file to the container

    ./deploy.sh

## Visit the site

<http://localhost:8080>


