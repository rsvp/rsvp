#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

TOMCAT_HOME="../apache-tomcat*"

# stop server

${TOMCAT_HOME}/bin/catalina.sh stop

# clean up

rm -rf ${TOMCAT_HOME}/webapps/*
rm -rf ${TOMCAT_HOME}/work/Catalina/*

# deploy

cd ${TOMCAT_HOME}/webapps
cp ${DIR}/target/rsvp.war ./ROOT.war
cd ${DIR}

# start server

${TOMCAT_HOME}/bin/catalina.sh start
