#!/bin/bash

# clean
rm -rf target/main/webapp/WEB-INF
mkdir -p target/main/webapp/WEB-INF/classes
mkdir -p target/main/webapp/WEB-INF/lib


# compile

javac -cp "src/main/resources/compile/javax.annotation-api-1.2.jar:src/main/resources/compile/javax.ws.rs-api-2.0.1.jar" "src/main/java/com/jesseandstefanie/rsvp/Rsvp.java" "src/main/java/com/jesseandstefanie/rsvp/JaxRS.java" -d "target/main/webapp/WEB-INF/classes"

if [[ $? -ne 0 ]]
then
  echo "Compilation failed. Not packaging war."
  exit 1
fi

# create sources tarball

tar cvfz ../rsvp.tar.gz * --exclude=target
mv ../rsvp.tar.gz target/main/webapp/

# Package

cp src/main/resources/compile/*.jar target/main/webapp/WEB-INF/lib/
cp src/main/resources/application/*.jar target/main/webapp/WEB-INF/lib/
cp -R src/main/webapp/* target/main/webapp/
cd target/main/webapp
jar cvf ../../rsvp.war .
